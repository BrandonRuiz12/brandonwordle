void main() {
  String m = 'juan';
  String i = 'juno';
 
  var ml = m.split('').asMap();
  
  var il = i.split('').asMap();
  
  
  var cm = ml.entries;
  var cmp = cm.map((e)=>Prueba(letra:e.value,numero:e.key));
  
  var ci = il.entries;
  var cip = ci.map((e)=>Prueba(letra:e.value,numero:e.key));
  
  var coin = cmp.toSet()
    .intersection(cip.toSet());
  
  List<Prueba> coinN=[];
  for ( var elemento in cip){
    print('elemento $elemento');
    coinN.addAll(
    cmp.where(
      (e)
      {print(e);
        return e.letra == elemento.letra && !coin.contains(e);})  
    );
  }
  
  print('Coincidencias exactas');
  print(coin);
  
  print('Coincidencias no exactas');
  print(coinN);
 // faltan las que no coinciden
}
 
class Prueba{
  final int numero;
  final String letra;
  
  const Prueba({required this.numero,required this.letra});
  @override
   bool operator ==(other) => 
     other is Prueba && 
     numero == other.numero && 
     letra == other.letra;
  @override
  int get hashCode => numero.hashCode ^ letra.hashCode;
  @override
  toString()=>'P($numero:$letra)';
}
